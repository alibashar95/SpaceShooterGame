package Input;

import Entities.Creatures.Player;
import Foundation.Handler;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener {

    private boolean[] keys;

    public boolean up, down, left, right, sprint;
    public boolean meleeAttack, rangedAttack;


    public KeyManager() {
        keys = new boolean[256];
    }

    public void tick() {
        sprint = keys[KeyEvent.VK_SHIFT];
        down = keys[KeyEvent.VK_DOWN];
        left = keys[KeyEvent.VK_LEFT];
        right = keys[KeyEvent.VK_RIGHT];
        up = keys[KeyEvent.VK_UP];
        meleeAttack = keys[KeyEvent.VK_X];
        rangedAttack = keys[KeyEvent.VK_C];

    }



    @Override
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }
}
