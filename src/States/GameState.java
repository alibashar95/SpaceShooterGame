package States;

import Entities.Creatures.Player;

import Foundation.Handler;
import Worlds.World1;

import java.awt.*;

public class GameState extends State {

    private Player player;
    private World1 world1;


    public GameState(Handler handler) {
        super(handler);
        world1 = new World1(handler, "res/worlds/world1.txt");
        handler.setWorld1(world1);
    }

    @Override
    public void tick() {
        world1.tick();
    }

    @Override
    public void render(Graphics g) {
        world1.render(g);
    }

}
