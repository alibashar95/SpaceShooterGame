package States;

import Foundation.Handler;
import Foundation.UI.ClickListener;
import Foundation.UI.UIImageButton;
import Foundation.UI.UIManager;
import GFX.Assets;
import GFX.ImageLoader;

import java.awt.*;

public class CharacterState extends State {

    private UIManager uiManager;
    private static int world = 1;

    public CharacterState(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);


        //Start Button
        uiManager.addObject(new UIImageButton(50, 200, 140, 60, Assets.startButton, new ClickListener() {
            @Override
            public void onClick() {
                handler.getMouseManager().setUiManager(null);
                State.setState(handler.getGame().gameState);
            }
        }));

        //Ship Button
        uiManager.addObject(new UIImageButton(50, 270, 140, 60, Assets.shipButton, new ClickListener() {
            @Override
            public void onClick() {

            }
        }));

        //Spells Button
        uiManager.addObject(new UIImageButton(50, 340, 140, 60, Assets.spellsButton, new ClickListener() {
            @Override
            public void onClick() {

            }
        }));

        //Exit Button
        uiManager.addObject(new UIImageButton(50, 410, 140, 60, Assets.exitButton, new ClickListener() {
            @Override
            public void onClick() {
                System.exit(1);
            }
        }));

    }

    @Override
    public void tick() {
        uiManager.tick();
        if (handler.getMouseManager().getUiManager() == null)
            handler.getMouseManager().setUiManager(uiManager);


    }

    @Override
    public void render(Graphics g) {

        g.setFont(new Font("Times New Roman",Font.PLAIN,20));
        g.drawImage(ImageLoader.loadImage("/textures/Backgrounds/character.png"), 0, 0, null);
        g.drawString("World " + world, 60, 200);
        g.drawImage(Assets.player, 70, 80, null);

        uiManager.render(g);
    }

    public static int getWorld() {
        return world;
    }

    public static void nextWorld () {
        world++;
    }
}
