package States;

import Foundation.Game;
import Foundation.Handler;
import Foundation.UI.ClickListener;
import Foundation.UI.UIImageButton;
import Foundation.UI.UIManager;
import GFX.Assets;
import GFX.ImageLoader;

import java.awt.*;

public class MenuState extends State {

    private UIManager uiManager;

    public MenuState(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);
        handler.getMouseManager().setUiManager(uiManager);

        // Start Button
        uiManager.addObject(new UIImageButton((handler.getWidth() / 2) - 75, 150, 200, 100, Assets.startButton, new ClickListener() {
            @Override
            public void onClick() {
                handler.getMouseManager().setUiManager(null);
                State.setState(handler.getGame().characterState);


            }
        }));

        // Options Button
        uiManager.addObject(new UIImageButton((handler.getWidth() / 2) - 75, 250, 200, 100, Assets.optionsButton, new ClickListener() {
            @Override
            public void onClick() {
                //Do something
            }
        }));

        // Exit Button
        uiManager.addObject(new UIImageButton((handler.getWidth() / 2) - 75, 350, 200, 100, Assets.exitButton, new ClickListener() {
            @Override
            public void onClick() {
                System.exit(1);
            }
        }));
    }

    @Override

    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics g) {
        uiManager.render(g);
    }

}
