package projectiles;

import Entities.Creatures.Creature;
import Foundation.Handler;
import GFX.ImageLoader;

import java.awt.*;

public class SplitShot extends Projectile {

    private final int BASE_DAMAGE = 30;
    private final int BASE_SPEED = 15;

    public SplitShot(Handler handler, Creature owner, char direction, int width, int height) {
        super(handler, owner, direction, width, height,0,0);
        active = true;
        damage = BASE_DAMAGE;
        speed = BASE_SPEED;
        texture = ImageLoader.loadImage("/textures/PNG/Lasers/laserBlue08.png");
    }

    public SplitShot(Handler handler, Creature owner, char direction, int width, int height, float addedX, float addedY) {
        super(handler, owner, direction, width, height, addedX, addedY);
        active = true;
        damage = BASE_DAMAGE;
        speed = BASE_SPEED;
        texture = ImageLoader.loadImage("/textures/PNG/Lasers/laserBlue08.png");
    }

    public SplitShot(Handler handler, Creature owner, char direction, int width, int height, int addedSpeed, int addedDamage) {
        super(handler, owner, direction, width, height, 0, 0);
        active = true;
        damage = BASE_DAMAGE + addedDamage;
        speed = BASE_SPEED + addedSpeed;
        texture = ImageLoader.loadImage("/textures/PNG/Lasers/laserBlue08.png");
    }

    public SplitShot(Handler handler, Creature owner, char direction, int width, int height, int addedSpeed, int addedDamage, float addedX, float addedY) {
        super(handler, owner, direction, width, height, addedX, addedY);
        active = true;
        damage = BASE_DAMAGE + addedDamage;
        speed = BASE_SPEED + addedSpeed;
        texture = ImageLoader.loadImage("/textures/PNG/Lasers/laserBlue08.png");
    }

//    public static void shoot(Handler handler, Creature owner, char direction,) {
//        handler.shootProjectile(new SplitShot(handler,owner,direction,));
//    }

    protected void move () {

    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {

    }
}
