package projectiles;

import Entities.Creatures.Creature;
import Entities.Entity;
import Foundation.Handler;
import GFX.ImageLoader;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class Projectile {

    protected Handler handler;
    protected Creature owner;
    protected float x ,y;
    protected float leftMove ,rightMove;
    protected char direction;
    protected int width ,height;
    protected int speed;
    protected Rectangle bounds;
    protected boolean active;
    protected int damage;
    protected BufferedImage texture;


    public Projectile(Handler handler, Creature owner, char direction, int width, int height, float addedX, float addedY) {
        this.handler = handler;
        this.x = owner.getX() + addedX;
        this.y = owner.getY() + addedY;
        this.height = height;
        this.width = width;
        this.owner = owner;
        this.direction = direction;
        active = true;
        bounds = new Rectangle((int) (owner.getX() + addedX), (int) (owner.getY() + addedY), this.width, this.height);
    }

    protected void move() {
        if (direction == 'U') {
            y -= speed;
            bounds.y -= speed;
        } else if (direction == 'D') {
                y += speed;
                bounds.y += speed;
        }
    }

    protected void moveX() {
        x += rightMove;
        bounds.x += rightMove;
        x -= leftMove;
        bounds.x -= leftMove;
    }

    protected void checkCollision() {

            for (Entity e : handler.getEntities()) {
                if (e.equals(owner))
                    continue;
                if (e.getCollisionBounds(0, 0).intersects(bounds)) {
                    e.hurt(damage);
                    active = false;
                    return;
                }
            }
            for (Creature e : handler.getCreatures()) {
                if (e.equals(owner))
                    continue;
                if (e.getCollisionBounds(0, 0).intersects(bounds)) {
                    e.hurt(damage);
                    active = false;
                    return;
                }
        }
    }


    public abstract void tick();

    public abstract void render(Graphics g);


    public boolean isActive() {
        return active;
    }
}
