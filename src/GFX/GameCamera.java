package GFX;

import Entities.Entity;
import Foundation.Handler;
import Tiles.Tile;

public class GameCamera {

    private Handler handler;
    private float xOffset, yOffset;
    private boolean moving;

    public GameCamera(Handler handler, float xOffset, float yOffset) {
        this.handler = handler;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
    }

    public void checkBlankSpace() {
        if (xOffset < 64) {
            xOffset = 64;
            moving = false;
        } else if (xOffset > (handler.getWorld1().getWidth() * Tile.TILEWIDTH - handler.getWidth())-64) {
            xOffset = handler.getWorld1().getWidth() * Tile.TILEWIDTH - handler.getWidth() - 64;
            moving = true;
        }

        if (yOffset < 0) {
            yOffset = 0;
            moving = false;
        }else if (yOffset > handler.getWorld1().getHeight() * Tile.TILEHEIGHT - handler.getHeight()) {
            yOffset = handler.getWorld1().getHeight() * Tile.TILEHEIGHT - handler.getHeight();
            moving = true;
        }
    }

    public void centerOnEntity(Entity e) {
        xOffset = e.getX() - handler.getWidth() / 2;
        yOffset = e.getY() - (handler.getHeight() / 2) - 100;
        checkBlankSpace();
    }

    public void move(float xAmount, float yAmount) {
        xOffset += xAmount;
        yOffset += yAmount;
        checkBlankSpace();
    }

    public float getxOffset() {
        return xOffset;
    }

    public void setxOffset(float xOffset) {
        this.xOffset = xOffset;
    }

    public float getyOffset() {
        return yOffset;
    }

    public void setyOffset(float yOffset) {
        this.yOffset = yOffset;
    }

    public boolean isMoving() {
        return moving;
    }
}
