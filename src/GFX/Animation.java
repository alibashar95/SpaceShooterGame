package GFX;

import java.awt.image.BufferedImage;

public class Animation {

    private int speed, index;
    private long lastTime, timer;
    private BufferedImage[] frames;
    private int repeatIfZero = 0;
    private boolean repeat = true;

    public Animation(int speed, BufferedImage[] frames) {
        this.speed = speed;
        this.frames = frames;
        index = 0;
        timer = 0;
        lastTime = System.currentTimeMillis();
    }

    public void tick() {
        if (repeatIfZero == 0) {

            timer += System.currentTimeMillis() - lastTime;
            lastTime = System.currentTimeMillis();

            if (timer > speed) {
                index++;
                timer = 0;
                if (index >= frames.length) {
                    index = 0;
                    if (!repeat)
                    repeatIfZero = 1;
                }


            }
        }
    }

    public void setRepeatIfZero(int repeatIfZero) {
        this.repeatIfZero = repeatIfZero;
    }

    public int getRepeatIfZero() {
        return repeatIfZero;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public BufferedImage getCurrentFrames() {
        return frames[index];
    }

}
