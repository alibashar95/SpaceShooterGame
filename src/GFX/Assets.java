package GFX;

import java.awt.image.BufferedImage;

public class Assets {

    private static final int WIDTH = 64, HEIGHT = 64;

    public static BufferedImage player, blank;

    public static BufferedImage[] startButton, optionsButton, exitButton, spellsButton, dronesButton, shipButton;
    


    public static void init() {
//        SpriteSheet sheet1 = new SpriteSheet(ImageLoader.loadImage("/textures/Tiles_64x64.png"));

        startButton = new BufferedImage[2];
        startButton[0] = ImageLoader.loadImage("/textures/buttons/start_button.png");
        startButton[1] = ImageLoader.loadImage("/textures/buttons/start_button_mouse.png");

        optionsButton = new BufferedImage[2];
        optionsButton[0] = ImageLoader.loadImage("/textures/buttons/optn.png");
        optionsButton[1] = ImageLoader.loadImage("/textures/buttons/optn_hover.png");

        exitButton = new BufferedImage[2];
        exitButton[0] = ImageLoader.loadImage("/textures/buttons/exit.png");
        exitButton[1] = ImageLoader.loadImage("/textures/buttons/exit_hover.png");

        spellsButton = new BufferedImage[2];
        spellsButton[0] = ImageLoader.loadImage("/textures/buttons/spells.png");
        spellsButton[1] = ImageLoader.loadImage("/textures/buttons/spells_hover.png");

        dronesButton = new BufferedImage[2];
        dronesButton[0] = ImageLoader.loadImage("/textures/buttons/helpers.png");
        dronesButton[1] = ImageLoader.loadImage("/textures/buttons/helpers_hover.png");

        shipButton = new BufferedImage[2];
        shipButton[0] = ImageLoader.loadImage("/textures/buttons/ship.png");
        shipButton[1] = ImageLoader.loadImage("/textures/buttons/ship_hover.png");

        player = ImageLoader.loadImage("/textures/PNG/playerShip1_blue.png");

    }

}
