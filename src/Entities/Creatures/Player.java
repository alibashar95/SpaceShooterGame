package Entities.Creatures;

import Entities.Entity;
import Foundation.Handler;
import Foundation.Timer;
import GFX.Animation;
import GFX.Assets;
import projectiles.BasicLazer;
import projectiles.Projectile;

import java.awt.*;

public class Player extends Creature {

    //Animations

    //Timers
    private Timer laserTimer = new Timer(400);

    private int currentXp = 0;
    private int targetXp = 200;
    private int level = 1;

    public Player(Handler handler, float x, float y) {
        super(handler, x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);

        bounds.x = 9*2;
        bounds.y = 10*2;
        bounds.width = 14*2;
        bounds.height = 21*2;

    }

    @Override
    public void tick() {

        if (currentXp > targetXp)
            levelUp();


        //Animations Tick

        //Movement
        getInput();
        move();

    }




    @Override
    public void die() {
        System.out.println("You lose");
    }

    private void getInput() {
        xMove = 0;
        yMove = 0;

        if (handler.getKeyManager().left)
            xMove = -speed;

        if (handler.getKeyManager().right)
            xMove = speed;

        if (handler.getKeyManager().up)
            yMove = -speed;

        if (handler.getKeyManager().down)
            yMove = speed;

        if (handler.getKeyManager().rangedAttack)
            shoot();


    }

    private void shoot() {
        laserTimer.start();
        if (laserTimer.cooldownComplete()) {
            laserTimer.reset();
            handler.shootProjectile(new BasicLazer(handler, this, 'U', 10, 25));
            handler.shootProjectile(new BasicLazer(handler, this, 'U', 10, 25, 50f,0));
        }
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(Assets.player, (int) (x - handler.getGameCamera().getxOffset()),
                (int) (y - handler.getGameCamera().getyOffset()), width, height, null);
    }

    public void addXp (int amount) {
        currentXp += amount;
    }

    private void levelUp() {
        level++;
        targetXp += 200;
    }

    public int getCurrentXp() {
        return currentXp;
    }

    public void setCurrentXp(int currentXp) {
        this.currentXp = currentXp;
    }

    public int getTargetXp() {
        return targetXp;
    }

    public void setTargetXp(int targetXp) {
        this.targetXp = targetXp;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
