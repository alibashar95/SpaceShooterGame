package Entities.Creatures;

import Entities.Entity;
import Foundation.Handler;
import Tiles.Tile;

import java.awt.image.BufferedImage;

public abstract class Creature extends Entity {

    protected static final float DEFAULT_SPEED = 6;
    protected static final int DEFAULT_CREATURE_WIDTH = 64, DEFAULT_CREATURE_HEIGHT = 64;

    protected float speed;


    protected float xMove, yMove;

    public Creature(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width, height);
        speed = DEFAULT_SPEED;
        xMove = 0;
        yMove = 0;
    }

    public void move() {
        if (!checkEntityCollisions(xMove, 0f))
            moveX();

        if (!checkEntityCollisions(0f, yMove))
            moveY();

        if (handler.getGameCamera().isMoving())
            y -= 1;

    }

    public void moveX() {
        if (xMove > 0) {
            int tempX = (int) (x + xMove + bounds.x + bounds.width) / Tile.TILEWIDTH;
            if (!collisionWithTile(tempX, (int) (y + bounds.y) / Tile.TILEHEIGHT) &&
                    !collisionWithTile(tempX, (int) (y + bounds.y + bounds.height) / Tile.TILEHEIGHT)) {
                x += xMove;

            } else {
                x = tempX * Tile.TILEWIDTH - bounds.x - bounds.width - 1;
            }

        } else if (xMove < 0) {
            int tempX = (int) (x + xMove + bounds.x) / Tile.TILEWIDTH;
            if (!collisionWithTile(tempX, (int) (y + bounds.y) / Tile.TILEHEIGHT) &&
                    !collisionWithTile(tempX, (int) (y + bounds.y + bounds.height) / Tile.TILEHEIGHT)) {
                x += xMove;
            } else {
                x = tempX * Tile.TILEWIDTH + Tile.TILEWIDTH - bounds.x;

            }
        }
    }



    public void moveY() {

        if (yMove < 0) {
            if (y > handler.getGameCamera().getyOffset()) {
                y += yMove;
            }


        } else if (yMove > 0) {
            if (y < handler.getGameCamera().getyOffset() + handler.getGame().getHeight() - height) {
                y += yMove;
            }
        }
    }

    protected boolean collisionWithTile(int x, int y) {
        return handler.getWorld1().getTile(x,y).isSolid();
    }

    protected BufferedImage getHealthBarFrame(BufferedImage[] healthBar) {
        if (health <= 0f) return healthBar[0];
        if (health >= 0f && health < 20f) return healthBar[2];
        if (health >= 20f && health < 40f) return healthBar[4];
        if (health >= 40f && health < 60f) return healthBar[6];
        if (health >= 60f && health < 80f) return healthBar[8];
        if (health >= 80f && health < 100f) return healthBar[10];
        if (health >= 100f && health < 120f) return healthBar[12];
        if (health >= 120f && health < 140f) return healthBar[14];
        if (health >= 140f && health < 180f) return healthBar[16];
        if (health >= 180f && health < 200f) return healthBar[18];
        if (health >= 200) return healthBar[20];

        return healthBar[20];
    }


    public float getxMove() {
        return xMove;
    }

    public void setxMove(float xMove) {
        this.xMove = xMove;
    }

    public float getyMove() {
        return yMove;
    }

    public void setyMove(float yMove) {
        this.yMove = yMove;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
}
