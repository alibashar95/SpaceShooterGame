package Entities.Creatures;

import Foundation.Handler;
import GFX.Animation;
import GFX.Assets;
import GFX.ImageLoader;
import Tiles.Tile;

import java.awt.*;
import java.awt.image.BufferedImage;

public class EnemyUfo extends Creature{

    private BufferedImage ufoTexture;
    private int xpWorth = 10;



    public EnemyUfo(Handler handler, float x, float y, int width, int height) {
        super(handler, x, y, width,height);
        ufoTexture = ImageLoader.loadImage("/textures/PNG/ufoBlue.png");
        bounds.width = width;
        bounds.height = height;
        bounds.x = 0;
        bounds.y = 0;

    }

    @Override
    public void tick() {
        moveX();
        moveY();
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(ufoTexture, (int) (x - handler.getGameCamera().getxOffset()),
                (int) (y - handler.getGameCamera().getyOffset()), width, height, null);
    }

    @Override
    public void die() {
        handler.getPlayer().addXp(xpWorth);
    }

}