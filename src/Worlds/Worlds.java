package Worlds;

import Entities.Creatures.Player;
import Foundation.Handler;
import Foundation.Utils;
import Tiles.Tile;

import java.awt.*;

public abstract class Worlds {

    protected Handler handler;
    protected int width, height;
    protected int spawnX, spawnY;
    protected int[][] tiles;
    protected Player player;
    protected boolean setCamera = false;

    protected Worlds(Handler handler, String path) {
        this.handler = handler;
        player = handler.getPlayer();

        loadWorld(path);
        player.setX(spawnX);
        player.setY(spawnY);
    }

    public Tile getTile(int x, int y) {
        if (x < 0 || y < 0 || x >= width || y >= height)
            return Tile.blankSpace;


        Tile t = Tile.tiles[tiles[x][y]];
        if (t == null)
            return Tile.blankSpace;

        return t;
    }

    protected void loadWorld(String path) {

        String file = Utils.loadFileAsString(path);
        String[] tokens = file.split("\\s+");
        width = Utils.parseInt(tokens[0]);
        height = Utils.parseInt(tokens[1]);
        spawnX = Utils.parseInt(tokens[2]);
        spawnY = Utils.parseInt(tokens[3]);

        tiles = new int[width][height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                tiles[x][y] = Utils.parseInt(tokens[(x + y * width) + 4]);
            }
        }
    }

    protected void renderWorld(Graphics g) {
        int xStart = (int) Math.max(0, handler.getGameCamera().getxOffset() / Tile.TILEWIDTH);
        int xEnd = (int) Math.min(width, (handler.getGameCamera().getxOffset() + handler.getWidth()) / Tile.TILEHEIGHT + 1);
        int yStart = (int) Math.max(0, handler.getGameCamera().getyOffset() / Tile.TILEHEIGHT);
        int yEnd = (int) Math.min(height, (handler.getGameCamera().getyOffset() + handler.getHeight()) / Tile.TILEHEIGHT + 1);

        for (int y = yStart; y < yEnd; y++) {
            for (int x = xStart; x < xEnd; x++) {
                getTile(x, y).render(g, (int) (x * Tile.TILEWIDTH - handler.getGameCamera().getxOffset()),
                        (int)(y * Tile.TILEHEIGHT - handler.getGameCamera().getyOffset()));
            }
        }
        handler.render(g);
    }

    public Handler getHandler() {
        return handler;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    protected void cameraTick() {
        handler.tick();
        handler.getGameCamera().move(0,-1);

        if (!setCamera) {
            handler.getGameCamera().setyOffset(height*Tile.TILEHEIGHT);
            setCamera = true;
        }
    }

    protected abstract void tick();

    protected abstract void render(Graphics g);

}
