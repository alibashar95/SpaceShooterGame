package Worlds;

import Entities.Creatures.EnemyUfo;
import Entities.Creatures.Player;
import Foundation.Handler;
import Foundation.Utils;
import Tiles.Tile;

import java.awt.*;

public class World1 extends Worlds {

    //Entities
    //private EnemyUfo snake1 = new EnemyUfo(handler, 500, 4 * Tile.TILEHEIGHT, 'L'); ??



    public World1(Handler handler, String path) {
        super(handler,path);
        handler.addCreature(new EnemyUfo(handler,300,1200,40,40));
        handler.addCreature(new EnemyUfo(handler,100,1300,40,40));
        handler.addCreature(new EnemyUfo(handler,400,1100,40,40));
        handler.addCreature(new EnemyUfo(handler,350,900,40,40));
        handler.addCreature(new EnemyUfo(handler,240,1050,40,40));
        handler.addCreature(new EnemyUfo(handler,300,780,40,40));

    }

    public void tick() {
        cameraTick();
    }

    public void render(Graphics g) {
        renderWorld(g);
    }

}
