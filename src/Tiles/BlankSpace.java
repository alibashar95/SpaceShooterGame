package Tiles;

import GFX.Assets;
import GFX.ImageLoader;

public class BlankSpace extends Tile {
    public BlankSpace(int id) {
        super(ImageLoader.loadImage("/textures/PNG/Meteors/meteorBrown_big1.png"), id);
    }

    @Override
    public boolean isSolid() {
        return false;
    }
}
