package Tiles;

import GFX.Assets;

public class SolidBlank extends Tile {
    public SolidBlank(int id) {
        super(Assets.blank, id);
    }
}
