package Foundation;

import Entities.Creatures.Creature;
import Entities.Creatures.Player;
import Entities.Entity;
import GFX.GameCamera;
import Input.KeyManager;
import Input.MouseManager;
import Worlds.World1;
import projectiles.Projectile;

import java.awt.*;
import java.util.ArrayList;

public class Handler {

    private Game game;
    private World1 world1;
    private Player player = new Player(this, 100, 200);
    private ArrayList<Entity> entities;
    private ArrayList<Creature> creatures;
    private ArrayList<Projectile> projectiles;

    public Handler(Game game) {
        this.game = game;
        entities = new ArrayList<Entity>();
        creatures = new ArrayList<Creature>();
        projectiles = new ArrayList<Projectile>();
        addEntity(player);
    }

    public void tick() {
        for (int i = 0; i < entities.size(); i++) {
            Entity e = entities.get(i);
            e.tick();
            if (!e.isActive()) {
                entities.remove(e);
            }
        }
        for (int i = 0; i < creatures.size(); i++) {
            Creature e = creatures.get(i);
            e.tick();
            if (!e.isActive()) {
                 creatures.remove(e);
            }
        }

        for (int i = 0; i < projectiles.size(); i++) {
            Projectile e = projectiles.get(i);
            e.tick();
            if (!e.isActive()) {
                projectiles.remove(e);
            }
        }
    }

    public void render(Graphics g) {
        for (Entity e : entities) {
            e.render(g);
        }
        for (Creature e : creatures) {
            e.render(g);
        }
        for (Projectile e : projectiles) {
            e.render(g);
        }
    }

    public void addEntity(Entity e) {
        entities.add(e);
    }

    public void addCreature(Creature e) {
        creatures.add(e);
    }

    public void shootProjectile(Projectile e) {
        projectiles.add(e);
    }

    public GameCamera getGameCamera() {
        return game.getGameCamera();
    }

    public KeyManager getKeyManager() {
        return game.getKeyManager();
    }

    public MouseManager getMouseManager() {
        return game.getMouseManager();
    }

    public int getWidth() {
        return game.getWidth();
    }

    public int getHeight() {
        return game.getHeight();
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public World1 getWorld1() {
        return world1;
    }

    public void setWorld1(World1 world1) {
        this.world1 = world1;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public ArrayList<Entity> getEntities() {
        return entities;
    }

    public void setEntities(ArrayList<Entity> entities) {
        this.entities = entities;
    }

    public ArrayList<Creature> getCreatures() {
        return creatures;
    }

    public void setCreatures(ArrayList<Creature> creatures) {
        this.creatures = creatures;
    }

}





