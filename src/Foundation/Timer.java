package Foundation;

public class Timer {

    private long timer;
    private long lastTime;
    private long cooldown;

    public Timer(long cooldown) {
        this.cooldown = cooldown;
        start();
    }

    public void start() {
        timer += System.currentTimeMillis() - lastTime;
        lastTime = System.currentTimeMillis();
    }

    public void setCooldown(long cooldown) {
        this.cooldown = cooldown;
    }

    public boolean cooldownComplete() {
        return !(timer < cooldown);
    }

    public void reset() {
        timer = 0;
    }

    public long getTimer() {
        return timer;
    }

    public long getCooldown() {
        return cooldown;
    }

    public void setTimer(long timer) {
        this.timer = timer;
    }


}
