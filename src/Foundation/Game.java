package Foundation;

import GFX.Assets;
import GFX.GameCamera;
import GFX.ImageLoader;
import Input.KeyManager;
import Input.MouseManager;
import States.CharacterState;
import States.GameState;
import States.MenuState;
import States.State;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class Game implements Runnable {


    private Window window;
    public String title;
    private int width, height;

    private boolean running = false;
    private Thread thread;

    private BufferStrategy bs;
    private Graphics g;

    //States
    public State gameState;
    public State menuState;
    public State characterState;

    //Input
    private KeyManager keyManager;
    private MouseManager mouseManager;

    //Camera
    private GameCamera gameCamera;

    //Handler
    private Handler handler;





    public Game(String title, int width, int height) {
        this.height = height;
        this.width = width;
        this.title = title;
        keyManager = new KeyManager();
        mouseManager = new MouseManager();

    }

    private void initialize() {
        window = new Window(title, width, height);
        window.getFrame().addKeyListener(keyManager);
        window.getFrame().addMouseListener(mouseManager);
        window.getFrame().addMouseMotionListener(mouseManager);
        window.getCanvas().addMouseListener(mouseManager);
        window.getCanvas().addMouseMotionListener(mouseManager);

        Assets.init();

        handler = new Handler(this);
        gameCamera = new GameCamera(handler, 0, 0);
        gameState = new GameState(handler);
        menuState = new MenuState(handler);
        characterState = new CharacterState(handler);

        State.setState(menuState);
    }

    private void tick() {
        keyManager.tick();

        if (State.getCurrentState() != null)
            State.getCurrentState().tick();

    }

    private void render() {
        bs = window.getCanvas().getBufferStrategy();
        if (bs == null){
            window.getCanvas().createBufferStrategy(3);
            return;
        }

        g = bs.getDrawGraphics();


        g.drawImage(ImageLoader.loadImage("/textures/Backgrounds/blue.png"), 0, 0, width, height, null);
//        g.clearRect(0,0,width,height);

        // render

        if (State.getCurrentState() != null)
            State.getCurrentState().render(g);

        // render

        bs.show();
        g.dispose();
    }

    public void run() {

        initialize();

        int fps = 60;
        double timePerTick = 1000000000 / fps;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();
        long timer = 0;
        int ticks = 0;


        while (running) {
            now = System.nanoTime();
            delta += (now - lastTime) / timePerTick;
            timer += now - lastTime;
            lastTime = now;

            if (delta >= 1) {
                tick();
                render();
                ticks++;
                delta--;
            }

            if (timer >= 1000000000) {
                System.out.println("FPS: " + ticks);
                ticks = 0;
                timer = 0;
            }

        }

        stop();

    }

    public MouseManager getMouseManager() {
        return mouseManager;
    }

    public KeyManager getKeyManager() {
        return keyManager;
    }

    public GameCamera getGameCamera() {
        return gameCamera;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


    public synchronized void start() {
        if (running)
            return;


        running = true;
        thread = new Thread(this);
        thread.start(); //starts run() method
    }

    public synchronized void stop() {
        if (!running)
            return;

        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
